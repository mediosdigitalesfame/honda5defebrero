<?php
require('formulario/constant.php');
?>
<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
 <head>
     
     <?php include('seguimientos.php'); ?>
     
	<title>Servicio Honda 5 de Febrero</title>
	<?php include('contenido/head.php'); ?>
	<?php include('scriptform.php'); ?>
 </head>
 <body>

 	<?php include('contenido/chat.php'); ?>
 	
     <div id="container">
		 <?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?>
         
         <div id="content">
             <div class="page-banner">         
				 <div class="container"> <h2>Servicio FAME</h2> </div>
			 </div>

			 <div class="about-box">
				 <div class="container">
				 	 <div class="col-md-3" align="center">
					 </div>     	 
                     <div class="col-md-6" align="center">
							 <?php include('formulario/formulariosub.php'); ?>
					 </div>     	 
			     </div>
			 </div>

 
		 <div class="section">
			 <div id="about-section">
                 <div class="welcome-box">
					 <div class="container">
						 <h1><span>NUESTRO SERVICIO ES EL MEJOR</span></h1><br>
						 <p align="justify">Contamos con la más alta tecnología para que tu <strong>HONDA</strong> permanezca en perfectas condiciones</p><br>
                 
						 <p align="justify">
						 - Servicio Express<br>
						 - Mantenimiento Correctivo y Preventivo<br>
						 - Alineación y Balanceo Computarizado<br>
						 - Hojalatería y Pintura<br>
						 - Detallado Automotriz</p><br><br>

						 <p align="left"><strong>Reparación de problemas mecánicos</strong></p><br>                        

						 <p align="justify">
						 - Motores<br>
						 - Transmisiones<br>
						 - Diferenciales<br>
						 - Reparación de sistemas electrónicos<br>
						 - Reparación de sistemas eléctricos<br>	
						 - Reparación de sistemas de emisiones<br>
						 - Reparación de sistemas de embragues y frenos<br>
						 - Alineación electrónica por computadora de dos ejes y cuatro planos<br>
						 - Balanceo por computadora de dos planos<br>
						 - Reparación de sistemas de suspensión y dirección
						 </p><br><br>


						 <p align="left"><strong>Horario de atención al cliente</strong><br>
						 <p align="justify">
						 - Lunes a Viernes 8:00 a 19:00 hrs<br>  
						 - Sábados 8:00 a 16:00 hrs
						 </p><br><br>

                         <br>

						 <h1><span>Precios</span></h1><br>
						 <div class="single-project-content">
							<img alt="" src="images/tablaprecios.jpg">                         </div>
                        
                         <br>

                	 </div>
				 </div>
             </div>
         </div>
         </div>
     </div>   
 	 
		<?php include('contenido/footer.php'); ?>

</body>
</html>