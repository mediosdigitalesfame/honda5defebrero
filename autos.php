<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
    
    <?php include('seguimientos.php'); ?>
    
	<title>Autos</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('contenido/chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
         <?php include('contenido/analytics.php'); ?>
	 <div id="content">
		 <div class="page-banner">
			 <div class="container">
				 <h2>Vehículos</h2>
			 </div>
		 </div>

		 <div class="portfolio-box with-3-col">
			 <div class="container">
				 <ul class="filter">
					 <li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todos</a></li>
					 <li><a href="#" data-filter=".auto">Autos</a></li>
					 <li><a href="#" data-filter=".suv">SUV's - Minivan</a></li>
					 <!--<li><a href="#" data-filter=".camioneta">Camioneta</a></li>
					 <li><a href="#" data-filter=".hibrido">Híbridos</a></li>--> 
				 </ul>

				 <div class="portfolio-container">

						 <div class="work-post suv"> 
							 <div class="work-post-gal">
								 <a href="pdfs/insight2019.pdf" target="_blank"><img alt="Insight" src="images/autos/original/insight2019.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> INSIGHT <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/insight2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv"> 
							 <div class="work-post-gal">
								 <a href="pdfs/brv2018.pdf" target="_blank"><img alt="BR-V" src="images/autos/original/brv2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> BR-V <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/brv2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/typer2018.pdf" target="_blank"> <img alt="Type R" src="images/autos/original/typer2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> TYPE R<sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/typer2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <img alt="FIT" src="images/autos/original/accord2018.png">
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> ACCORD <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/accord2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					      <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/civic2019.pdf" target="_blank"><img alt="Civic" src="images/autos/original/civic2019.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CIVIC <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/civic2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <span><a href="pdfs/civiccoupe2018.pdf" target="_blank"><img alt="Civic Coupe" src="images/autos/original/civiccoupe2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CIVIC COUPE <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/civiccoupe2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/city2019.pdf" target="_blank"><img alt="City" src="images/autos/original/city2019.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CITY <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/city2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post auto">
							 <div class="work-post-gal">
								 <a href="pdfs/fit2019.pdf" target="_blank"><img alt="FIT" src="images/autos/original/fit2019.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> FIT <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/fit2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/HRV2019.pdf" target="_blank"><img alt="HR-V" src="images/autos/original/hrv2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> HR-V <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/HRV2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/odyssey2018.pdf" target="_blank"><img alt="FIT" src="images/autos/original/crv2018.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> CR-V<sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/odyssey2018.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/pilot2019.pdf" target="_blank"><img alt="FIT" src="images/autos/original/pilot2019.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> PILOT<sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/pilot2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					     <div class="work-post suv">
							 <div class="work-post-gal">
								 <a href="pdfs/ODYSSEY2019.pdf" target="_blank"><img alt="FIT" src="images/autos/original/odyssey2019.png"></a>
							 </div>
							 <div class="work-post-content">
								 <h2> <strong> <em> ODYSSEY <sup>&reg;</sup> <font color="#DD1707">2019</font> </em> </strong></h2> </font>
								 <span><a href="pdfs/ODYSSEY2019.pdf" target="_blank">Ficha técnica</a></span>
							 </div>
					     </div>

					 </div>
	             </div>
		     </div>
         </div>
     </div>
 <!-- FIN VEHICULOS -->
 	
 <?php include('contenido/footer.php'); ?>

 </body>
</html>