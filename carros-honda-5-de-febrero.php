<!doctype html>

<html lang="es" xml:lang="es" class="no-js">

 <head>
     
     <?php include('seguimientos.php'); ?>

	 <title>Contacto</title>

	 <?php include('contenido/head.php'); ?>

 </head>

 <body>

 <?php include('contenido/chat.php'); ?>

	<div id="container">

		 <?php include('contenido/header.php'); ?>

		 <?php include('contenido/analytics.php'); ?>

 

		 <div id="content">

			 <div class="page-banner"> <div class="container"> <h2>Contáctanos</h2> </div> </div>

 

			 <div class="contact-box">

				 <div class="container">

					 <div class="row">

                          <div class="col-md-6" align="center">

                                 <div class="container">

					                 <div class="col-md-12" >

								         <?php include('form.php'); ?>

                                     </div>

                                 </div>

                             </div>



						 <div class="col-md-3">

							 <div class="contact-information">

								 <h3>Información de Contacto</h3>

								 <ul class="contact-information-list">

									 <li><span><i class="fa fa-home"></i>Av. 5 de Febrero #1502</span> <span>Colonia San Pablo,  </span><span>C.P. 76130</span><span> Querétaro, QRO.</span></li>

									 <li><span><i class="fa fa-phone"></i><strong>(442) 210 2404</strong></span></li>

									 <li><a href="#"><i class="fa fa-envelope"></i>recepcion5defebrero@famemarquesa.com</a></li>

                                    	<h3>Whatsapp</h3>

                       				 <li><span><i class="fa fa-whatsapp">

                                         </i><strong> Recepcion    4433693559 </strong></span></li>

                                                

                                     <li><span><i class="fa fa-whatsapp">

                                         </i><strong> Postventa    4433694057 </strong></span></li>

								 </ul>

							 </div>

						 </div>



						 <div class="col-md-3">

							 <div class="contact-information">

								 <h3>Horario de Atención</h3>

								 <p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en FAME Honda; te escuchamos y atendemos de manera personalizada. </p>

								 <p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>

								 <p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

							 </div>

						 </div>

 

							

 

					</div>

				</div>

<?php include('contenido/vehiculos.php'); ?>

			</div>



		</div> 



		<br>

        

		<?php include('contenido/footer.php'); ?>

     </div> 			

	

</body>

</html>