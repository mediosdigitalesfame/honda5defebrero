<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Promociones</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

    <?php include('contenido/chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>

         <div class="page-banner">         
             <div class="container"> <h2><strong>Promociones</strong></h2> </div>
                 </div>

                 <div class="single-project-page">
                     <div class="container">
                         <div class="row">
                    
                         <span class="single-project-content">

                            <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/xproximashonda.jpg"></a>
                         </span> 

                              

                         <!-- PROMO 1 ================================================= 

                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/CITY.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>CITY</h2>
                             <p align="justify">

                             	Vigencia del 01 al 31 de julio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $2,633 en la compra de Honda® City® en versión LX MT modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $2,632.25, con un CAT de 42.76% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. 0% de comisión por apertura en la compra de Honda® City® versión LX MT, LX CVT y EX CVT modelo 2017 y 2018, con un pago desde 10% (diez) de enganche y plazos de 12 a 72, con un CAT de 40.27% sin IVA promedio informativo para modelo 2017 y CAT de 48.81% sin IVA promedio informativo para modelo 2018, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. 1er. Servicio Gratis. Aplica en la compra de Honda® City® versión LX MT, LX CVT y EX CVT modelo 2017 y 2018, adquiridos durante el periodo de vigencia y en el primer servicio del programa de mantenimiento especificado en la póliza de garantía 5,000 Km o 6 meses lo que ocurra primero (cambio de aceite de motor, lavado y aspirado, las revisiones de: bandas de propulsión, conductos y manguera de frenos, componentes de suspensión, componentes de dirección, conexiones y líneas de combustible, ejes de mando y flechas, conexiones y mangueras de enfriamiento, sistema de escape, acumulador y conexiones) o 10,000 kilómetros o 12 meses lo que ocurra primero (cambio de aceite y filtro de motor, lavado y aspirado, las revisiones de: bandas de propulsión, conductos y manguera de frenos, componentes de suspensión, componentes de dirección, conexiones y líneas de combustible, ejes de mando y flechas, conexiones y mangueras de enfriamiento, sistema de escape, acumulador y conexiones) . Válido únicamente en los concesionarios Autorizados Honda®. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div> <br><br>


                          <!-- PROMO 1 =================================================  


                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/CRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>CR-V</h2>
                             <p align="justify">

                             	<strong>
                             		Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $4,054.61 en la compra de Honda® CR-V® en versión EX modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $4,054.61, con un CAT de 26.71% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.


                             </p><br>
                         </div> <br><br>

                         <!-- PROMO 1 =================================================  

                        <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/TYPE-R.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>TYPE-R</h2>
                             <p align="justify">

                             	<strong>
                                    Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Type-R® modelo 2017 y 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 33.96% sin IVA promedio informativo para modelo 2018 y CAT de 33.32% sin IVA promedio informativo para modelo 2017, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.</strong>

                             </p><br>
                         </div> <br><br>

                         <!-- PROMO 1 =================================================  

                         <span class="single-project-content">
                             <a href="contacto.php"> <img alt="Honda promociones " src="promos/PILOT.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                   
                         <div class="col-md-12">
                             <h2>PILOT</h2>
                             <p align="justify">

                             	<strong>
                                    Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Pilot® en versión Touring modelo 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 31.09% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.</strong>

                             </p><br>
                         </div> <br><br>


                         <!-- PROMO 1 =================================================  
                    
                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones" src="promos/BRV.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

				      	     <h2>BRV</h2>
				      	     <p align="tify"> 
				      	     	Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. Mensualidades desde $3,335.16 en la compra de Honda® BR-V® en versión UNIQ modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $3,335.16 sin seguro incluido. CAT de 33.80 % sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
				      	     </p><br>
	                     </div><br><br> 

                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones" src="promos/ODYSSEY.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

				      	     <h2>ODDYSEY</h2>
				      	     <p align="justify"> 
				      	     	Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Odyssey® en versión Touring modelo 2019, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 25.84% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
				      	     </p><br>
	                     </div><br><br>

                         <span class="single-project-content">
                              <a href="contacto.php"> <img alt="Honda promociones" src="promos/ACCORD.jpg"></a>
                         </span>   

                         <span class="single-project-content">
                         </span>                        
                     
                         <div class="col-md-12">

                             <h2>ACCORD</h2>
                             <p align="justify"> 
                                Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia o lo que suceda primero. Mensualidades desde $4,983.00 pesos en la compra de Honda® Accord® en versión Sport Plus modelo 2018, con un pago desde 50% (cincuenta) de enganche y plazos de 72 meses de $4,983.00, con un CAT de 26.75% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano. Tasa desde 11.99%. Vigencia del 01 al 31 de Julio de 2018 o hasta agotar existencia lo que suceda primero. Tasas desde 11.99% en la compra de Honda® Accord® en versiones Ex CVT, Sport Plus y Touring modelo 2018, con un pago desde 40% de enganche y plazos desde 12 a 72 meses, con un CAT de 32.81% sin IVA promedio informativo, que considera el costo del seguro de vida, seguro de daños y comisión por apertura de crédito. Crédito exclusivo con Honda Finance. Aplican restricciones. No incluye accesorios del vehículo. No aplica con otras promociones. Para mayor información acerca de requisitos y opciones de financiamiento visite su Distribuidor autorizado Honda® más cercano.
                             </p><br>
                         </div><br><br> -->
	 
     </div>
     </div>
     </div>
                                
<<?php include('contenido/footer.php'); ?>

                            
                     </div>
                 </div>
             </div>

 </body>
</html>