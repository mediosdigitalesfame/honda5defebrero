<!doctype html>
<html lang="es" class="no-js">
 <head>
     
     <?php include('seguimientos.php'); ?>
     
	 <title>Garantía Extendida</title>
	 <?php include('contenido/head.php'); ?>
 </head>

 <body>

 	<?php include('contenido/chat.php'); ?>

 	 <div id="container">
		 <?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?>
 
		 <div id="content">
 
			 <div class="page-banner">         
				 <div class="container"> <h2>Garantía Extendida</h2> </div>
			 </div>

			<div class="about-box">
 
		     <div class="section">
			     <div id="about-section">
				     <div class="welcome-box">
					     <div class="container">
						     <h1><span>EXTENSIÓN DE GARANTÍA</span></h1><br>
						     <p align="justify">La Extensión de Garantía inicia al término de la garantía original del automóvil Honda (3 años ó 60,000 km. lo que ocurra primero) y hasta por el plazo contratado que puede ser de:</p><br>
                 
							 <p align="justify">
							 - 12 meses ó 20,000 km. lo que ocurra primero<br>
							 - 24 meses ó 40,000 km. lo que ocurra primero<br>
							 - 36 meses ó 60,000 km. lo que ocurra primero*<br>
							 </p><br>
							 <p align="center">*Aplica únicamente en año-modelo actual.</p><br><br>

							 <p align="justify">
							 El objetivo es extender la garantía de su nuevo automóvil Honda hasta por 6 años ó 120,000 Km. Cubre la reparación de los conjuntos afectados, piezas y costos de mano de obra. El trámite debe realizarse antes de los 3 meses a partir de la fecha de facturación y antes de los 10,000 km recorridos. El trámite debe realizarse antes de los 3 meses a partir de la fecha de facturación y antes de los 10,000 km recorridos.
							 </p><br><br>

							 <p align="left"><strong>Certificado de garantía, cobertura de tren motriz</strong></p><br>   
							 <p align="justify">
							 El Certificado de Garantía inicia al término de la garantía original del automóvil Honda (3 años ó 60,000 km. lo que ocurra primero) y hasta por el plazo contratado que puede ser de:
							 </p><br>

							 <p align="justify">
							 - 12 meses ó 20,000 km, lo que ocurra primero<br>
							 - 24 meses ó 40,00 km, lo que ocurra primero<br><br>
							 </p><br>

							 <p align="justify">Es el documento que certificará la garantía de su automóvil por defectos de materiales y mano de obra en tren motríz (ver clausulado). El Certificado de Garantía es para los autos con uso no mayor a 5 años de antigüedad al actual (fecha de facturación) y con menos de 100,000 km.</p><br><br>
                             
                             <h1><span>Precios</span></h1><br>

							 <div class="single-project-content">
								 <img alt="" src="images/precio-garantia.png">
                         	 </div>
                	     </div>
				     </div>
				     </div>
				     </div>
				     </div>
				     </div>
				     </div>

                

		<?php include('contenido/footer.php'); ?>

</body>
</html>